//
//  PlaylistViewController.swift
//  SoundCheck
//
//  Created by Jake on 9/24/16.
//  Copyright © 2016 Jake. All rights reserved.
//

import UIKit
import RealmSwift

class PlaylistViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tableView: UITableView!
    
    let realm = try! Realm()
    var songList = [Song]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for i in realm.objects(Song.self) {
            songList.append(i)
        }
        
        self.view.backgroundColor = UIColor.clearColor()

        let navBar = SoundCheckNavigationBar()
        navBar.build(self.view.frame.width)
        let navItem = UINavigationItem(title: "SoundCheck")
        navItem.titleView = UIImageView(image: UIImage(named: "logo-small"))
        navBar.setItems([navItem], animated: false)
        self.view.addSubview(navBar)
        
        tableView = UITableView(frame: CGRectMake(0, navBar.frame.height, self.view.frame.width, self.view.frame.height - navBar.frame.height - (self.tabBarController?.tabBar.frame.height)!))
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.clearColor()
        tableView.separatorColor = SoundCheckColors.backgroundBlack
        self.view.addSubview(tableView)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let listenView = ListenViewController()
        listenView.songIndex = indexPath.row
        listenView.songList = self.songList
        listenView.modalPresentationStyle = .OverCurrentContext
        presentViewController(listenView, animated: true, completion: nil)
        return
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songList.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1 // hardCodedSongs.count
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: .Subtitle, reuseIdentifier: "HeartedCell")
        
        cell.backgroundColor = SoundCheckColors.backgroundBlack
        cell.textLabel?.textColor = UIColor.whiteColor()
        cell.detailTextLabel?.textColor = UIColor.whiteColor()
        cell.textLabel?.text = songList[indexPath.row].name
        cell.detailTextLabel?.text = songList[indexPath.row].artist
        cell.selectionStyle = .None
        
        return cell
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let songToRemove = songList[indexPath.row]
            try! realm.write {
                realm.delete(songToRemove)
            }
            
            songList.removeAtIndex(indexPath.row)
            
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    func reload() {
        songList = Array(realm.objects(Song.self))
//        songList.removeAll()
//        for i in realm.objects(Song.self) {
//            songList.append(i)
//        }
        
        self.tableView.reloadData()
    }
}
