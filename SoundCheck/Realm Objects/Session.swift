//
//  Sessions.swift
//  SwipeMusic
//
//  Created by Jake on 9/16/16.
//  Copyright © 2016 Jake. All rights reserved.
//

import Foundation
import RealmSwift

class Session: Object {
    dynamic var user = ""
    dynamic var uri = ""
    dynamic var name = ""
    
    override class func primaryKey() -> String? {
        return "uri"
    }
}