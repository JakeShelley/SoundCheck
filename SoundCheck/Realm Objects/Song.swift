//
//  Song.swift
//  SwipeMusic
//
//  Created by Jake on 9/15/16.
//  Copyright © 2016 Jake. All rights reserved.
//

import Foundation
import RealmSwift

class Song: Object {
    dynamic var uri = ""
    dynamic var artPath = NSData()
    dynamic var name = ""
    dynamic var artist = ""
    
    override class func primaryKey() -> String? {
        return "uri"
    }
}
