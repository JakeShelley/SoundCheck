//
//  User.swift
//  SwipeMusic
//
//  Created by Jake on 9/15/16.
//  Copyright © 2016 Jake. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object {
    dynamic var id = 0
    let songs = List<Song>()
    let listenedSessions = List<Session>()
    dynamic var firstLogin = 0
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
