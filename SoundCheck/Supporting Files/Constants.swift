//
//  Constants.swift
//  SoundCheck
//
//  Created by Jake on 9/20/16.
//  Copyright © 2016 Jake. All rights reserved.
//

import Foundation

struct SoundCheckColors {
    static let yellow = UIColor(red:0.91, green:0.77, blue:0.26, alpha:1.0)
    static let white = UIColor(red:0.96, green:0.97, blue:0.98, alpha:1.0)
    static let backgroundBlack = UIColor.blackColor() //UIColor(red:0.07, green:0.07, blue:0.07, alpha:1.0)
    static let darkGray = UIColor(red:0.16, green:0.16, blue:0.16, alpha:1.0)
    static let red = UIColor(red:0.91, green:0.05, blue:0.29, alpha:1.0)
    static let blue = UIColor(red:0.13, green:0.79, blue:0.91, alpha:1.0)
    static let gray = UIColor(red:0.85, green:0.85, blue:0.85, alpha:1.0)
    static let spotifyGreen = UIColor(red:0.18, green:0.75, blue:0.35, alpha:1.0)
    static let smokeWhite = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
}

// Lol at using singletons this is a mess :)
class UserSingleton {
    static let sharedInstance = User()
}
