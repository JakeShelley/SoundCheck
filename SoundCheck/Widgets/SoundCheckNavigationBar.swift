//
//  SoundCheckNavigationBar.swift
//  SoundCheck
//
//  Created by Jake on 9/20/16.
//  Copyright © 2016 Jake. All rights reserved.
//

import UIKit

class SoundCheckNavigationBar: UINavigationBar {
    
    func build(containerWidth: CGFloat) {
        self.frame = CGRectMake(0, 0, containerWidth, 55)
        self.barTintColor = SoundCheckColors.backgroundBlack
        self.tintColor = UIColor.whiteColor()
        self.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "AvenirNext-Heavy", size: 18)!]
        
        let bottomBar = UIView(frame: CGRect(x: 0, y: self.frame.height - 2, width: self.frame.width, height: 2))
        bottomBar.backgroundColor = SoundCheckColors.darkGray
        self.addSubview(bottomBar)
    }
}
