//
//  TabBarController.swift
//  SoundCheck
//
//  Created by Jake on 9/24/16.
//  Copyright © 2016 Jake. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.barTintColor = SoundCheckColors.backgroundBlack
        tabBar.tintColor = SoundCheckColors.yellow
        
        let sb = UIStoryboard(name: "Main", bundle: nil)
        
        let listenView = sb.instantiateViewControllerWithIdentifier("listen")
        listenView.tabBarItem.tag = 0
        listenView.title = "Listen"
        listenView.tabBarItem.image = UIImage(named: "headphone")
        listenView.tabBarItem.selectedImage = UIImage(named: "headphone-fill")
        
        let sessionsView = sb.instantiateViewControllerWithIdentifier("sessions")
        sessionsView.tabBarItem.tag = 1
        sessionsView.title = "Sessions"
        sessionsView.tabBarItem.image = UIImage(named: "record")
        sessionsView.tabBarItem.selectedImage = UIImage(named: "record-fill")

        let profileView = sb.instantiateViewControllerWithIdentifier("profile")
        profileView.tabBarItem.tag = 2
        profileView.title = "Profile"
        profileView.tabBarItem.image = UIImage(named: "user")
        profileView.tabBarItem.selectedImage = UIImage(named: "user-fill")
        
        self.viewControllers = [listenView, sessionsView, profileView]
        
        self.selectedViewController = sessionsView
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        if (item.tag == 0) {
            let listenView = self.viewControllers![0] as! PlaylistViewController
            if (listenView.tableView != nil) {
                listenView.reload() 
            }
        }
    }
}
