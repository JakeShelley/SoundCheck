//
//  AppDelegate.swift
//  SoundCheck
//
//  Created by Jake on 9/22/16.
//  Copyright © 2016 Jake. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        UIApplication.sharedApplication().setStatusBarStyle(.LightContent, animated: false)
        
        
        // Realm Migration
        Realm.Configuration.defaultConfiguration = Realm.Configuration(
            schemaVersion: 1,
            migrationBlock: { migration, oldSchema in
                if (oldSchema < 1) {
                    
                }
            }
        )
        
        
        let realm = try! Realm()
        
        if (realm.objects(User.self).first == nil) {
            let user = User()
            user.id = 0
            try! realm.write {
                realm.add(user)
            }
        }
        
        // Set up shared authentication information
        let auth = SPTAuth.defaultInstance()
        auth.clientID = "26ae0fddb75d425d90d8866d994a3d7a"
        auth.requestedScopes = [SPTAuthStreamingScope]
        auth.redirectURL = NSURL(string: "swipe-music://callback")
        auth.tokenSwapURL = NSURL(string: "http://www.jacobshelley.com:4000/swap")
        auth.tokenRefreshURL = NSURL(string: "http://wwww.jacobshelley.com:4000/refresh")
        auth.sessionUserDefaultsKey = "SessionUserDefaultsKey"
        
        return true
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        print("test")
        let auth = SPTAuth.defaultInstance()
        let authCallback = {(error: NSError!, session: SPTSession!) in
            if (error != nil) {
                print("auth error")
                return
            }
            
            auth.session = session
            // Notifcation stuff
            } as SPTAuthCallback
        
        if (auth.canHandleURL(url)) {
            auth.handleAuthCallbackWithTriggeredAuthURL(url, callback: authCallback)
            return true
        }
        
        return false
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
        
}

