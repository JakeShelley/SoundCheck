//
//  SessionsViewController.swift
//  SwipeMusic
//
//  Created by Jake on 9/16/16.
//  Copyright © 2016 Jake. All rights reserved.
//

import UIKit
import RealmSwift

class SessionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SPTAudioStreamingDelegate, SPTAudioStreamingPlaybackDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var sessions = [Session]()
    let realm = try! Realm()
    let reuseIdentifier = "SessionCell"
    var player: SPTAudioStreamingController!
    let temporarySessions = [
        ["name": "Top Tracks", "user": "1251363694", "uri": "7BOSK4p75TXKi6TGEUhkTV"],
        ["name": "New & Vibey", "user": "1251363694", "uri": "0AxTXAczLWfp1obOVMpxEs"],
        
        ["name": "Pop Drops", "user": "1251363694", "uri": "3T01TlGlbSvOl7bRPOEy9I"],
        ["name": "Republic Records Sampler", "user": "1251363694", "uri": "6MYr6jcRFKADm9Lic5lr14"],
        ["name": "Party Starters", "user": "1251363694", "uri": "2EL7q072uphq8sMlqAjyFa"],
        ["name": "Can I Kick It?", "user": "1251363694", "uri": "2kRKGPvmcVvNrDXshrwtnc"],
        ["name": "Windows Down Rock", "user": "1251363694", "uri": "2UbvT9TX6B2bSBhODdNhxo"],
        ["name": "Espresso Shot", "user": "1251363694", "uri": "75B0XPCl8p2uHSfCHL4I5W"],
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = SoundCheckColors.backgroundBlack
        self.tableView.backgroundColor = SoundCheckColors.backgroundBlack
        self.tableView.separatorColor = SoundCheckColors.darkGray
        
        let navBar = SoundCheckNavigationBar()
        navBar.build(self.view.frame.width)
        let navItem = UINavigationItem(title: "SoundCheck")
        navItem.titleView = UIImageView(image: UIImage(named: "logo-small"))
        navBar.setItems([navItem], animated: false)
//        self.navigationItem.titleView = UIImageView(image: UIImage(named: "logo"))
        
        self.view.addSubview(navBar)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        if (self.player == nil) {
            self.player = SPTAudioStreamingController.sharedInstance()
        }
        
        // This is a temporary function until requests can be made from the backend
        try! realm.write {
            for i in temporarySessions {
                if (realm.objectForPrimaryKey(Session.self, key: i["uri"]!) != nil) {
                    continue
                }
                
                let session = Session()
                session.name = i["name"]!
                session.uri = i["uri"]!
                session.user = i["user"]!
                
                realm.add(session)
            }
        }
        
        tableView.reloadData()
        // End of temporary code (famous last words)
        
        self.sessions = Array(realm.objects(Session))
        self.player = SPTAudioStreamingController.sharedInstance()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // I hate this
        if (realm.objectForPrimaryKey(User.self, key: 0)?.firstLogin == 0) {
            let alertController = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.Alert)
            
            let myString  = "Ready to discover new music?"
            var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSFontAttributeName:UIFont(name: "Avenir-Heavy", size: 21)!])
            alertController.setValue(myMutableString, forKey: "attributedTitle")
            
            // Change Message With Color and Font
            
            let message  = "Select a session and listen away.\nSwipe right if you like what you hear, left if you don't."
            var messageMutableString = NSMutableAttributedString()
            messageMutableString = NSMutableAttributedString(string: message as String, attributes: [NSFontAttributeName:UIFont(name: "Avenir", size: 16.0)!])
            alertController.setValue(messageMutableString, forKey: "attributedMessage")

            let action = UIAlertAction(title: "Got it!", style: UIAlertActionStyle.Default, handler: nil)
            alertController.addAction(action)
            
            let backView = alertController.view.subviews.last?.subviews.last
            backView?.layer.cornerRadius = 10.0
            backView?.backgroundColor = SoundCheckColors.smokeWhite
            self.presentViewController(alertController, animated: true, completion: {
                try! self.realm.write {
                    self.realm.objectForPrimaryKey(User.self, key: 0)?.firstLogin = 1
                }
            })
        }
    }

    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
        let headerImage = UIImageView(frame: CGRect(x: 8, y: 5, width: 20, height: 20))
        header.backgroundColor = SoundCheckColors.darkGray
        headerImage.tintColor = SoundCheckColors.yellow
        let label = UILabel(frame: CGRect(x: 35, y: 0, width: self.view.frame.width - 20, height: 30))
        label.textColor = UIColor.whiteColor()
        
        if (section == 0) {
            headerImage.image = UIImage(named: "fire")
            label.text = "Trending"
        } else {
            headerImage.image = UIImage(named: "heart")
            label.text = "For You"
        }
        
        header.addSubview(headerImage)
        header.addSubview(label)
 
        return header
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0) {
            return 2
        }
        
        return sessions.count - 2
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier)
        if (cell == nil) {
            cell = UITableViewCell()
        }
        
        cell?.selectionStyle = .None
        cell?.backgroundColor = SoundCheckColors.backgroundBlack
        cell?.textLabel?.textColor = UIColor.whiteColor()
        
        // force bottom cells to be "For You" sessions
        if (indexPath.section == 1) {
            cell?.textLabel?.text = sessions[indexPath.row + 2].name
            return cell!
        }
        
        cell?.textLabel!.text = sessions[indexPath.row].name
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let destVC = PlayerViewController()
        
        if (indexPath.section == 1) {
            if (indexPath.row > 9) {
                return
            }
            
            destVC.title = sessions[indexPath.row + 2].name
            destVC.uriString = sessions[indexPath.row + 2].user + ":playlist:" + sessions[indexPath.row + 2].uri
            print(destVC.uriString)
        } else {
            destVC.title = sessions[indexPath.row].name
            destVC.uriString = sessions[indexPath.row].user + ":playlist:" + sessions[indexPath.row].uri
        }
        
        destVC.delegate = self
        destVC.modalPresentationStyle = .OverCurrentContext
        presentViewController(destVC, animated: true, completion: nil)
    }
    
    func backFromSession() {
        // set as delegate so that if user leaves too early it can be caught and stopped
        self.player.delegate = self
        self.player.playbackDelegate = self
    }
    
    // Incase user quits out of session to fast
    func audioStreaming(audioStreaming: SPTAudioStreamingController!, didChangePosition position: NSTimeInterval) {
        self.player.setIsPlaying(false, callback: { error in
            do {
                try self.player.stop()
            } catch {
                print(error)
            }
        })
    }
}
