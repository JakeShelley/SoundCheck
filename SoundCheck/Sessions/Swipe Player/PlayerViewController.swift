//
//  PlayerViewController.swift
//  SwipeMusic
//
//  Created by Jake on 9/7/16.
//  Copyright © 2016 Jake. All rights reserved.
//

import UIKit
import RealmSwift

class PlayerViewController: UIViewController, SPTAudioStreamingDelegate, SPTAudioStreamingPlaybackDelegate {

    let realm = try! Realm()
    
    var swipeImage: UIImageView!
    
    var delegate: SessionsViewController!
    var player: SPTAudioStreamingController!
    var loadedCards = NSMutableArray()
    var initialLoad = true // Initial loading of the cards
    var changingPosition = false
    var curr: DraggableView!
    var draggableViewBackground: DraggableViewBackground!
    var navBar: SoundCheckNavigationBar!
    var uriString: String!
    var infoView: UIView!
    var loadingImage: UIImageView!
    var replaying = false // Make sure replay button doesn't get hit twice
    var dismissInProgress = false
    
    // Album image array has as many nil's as the number of cards
    var albumImageArray: [UIImage?] = [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navBar = SoundCheckNavigationBar()
        self.navBar.build(self.view.frame.width)
        let navItem = UINavigationItem(title: self.title!)
        let closeButton = UIBarButtonItem(barButtonSystemItem: .Stop, target: nil, action: #selector(hidePlayer))
        navItem.leftBarButtonItem = closeButton
        closeButton.setBackButtonTitlePositionAdjustment(UIOffsetMake(40, 40), forBarMetrics: .Default)
        navBar.setItems([navItem], animated: false)
        self.view.addSubview(navBar)
        
        self.view.backgroundColor = SoundCheckColors.backgroundBlack
        createInfoView(true)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.draggableViewBackground = DraggableViewBackground(frame: self.view.frame)
        self.draggableViewBackground.frame.origin.y = -self.draggableViewBackground.frame.height
        self.draggableViewBackground.build(self)
        rotateImage()
    }
    
    func handleNewSession() {
        let auth = SPTAuth.defaultInstance()
        
        if (self.player == nil) {
            self.player = SPTAudioStreamingController.sharedInstance()
        }

        do {
            try self.player.startWithClientId(auth.clientID, audioController: nil, allowCaching: true)
            self.player.delegate = self
            self.player.playbackDelegate = self
            self.player.diskCache = SPTDiskCache(capacity: 1024 * 1024 * 64)
            self.player.loginWithAccessToken(auth.session.accessToken)
            self.view.addSubview(draggableViewBackground)
        } catch {
            // Could not start up (improve error handling)
            print("error could not start client")
        }
        
        self.view.bringSubviewToFront(navBar)
    }
    
    func createInfoView(isLoading: Bool) {
        // ===== Prime refactor target! ======
        var CARD_HEIGHT = CGFloat(self.view.frame.height/1.50)
        var CARD_WIDTH = CGFloat(self.view.frame.width/1.1)
        
        // Set height for iPhone 5/4
        if (self.view.frame.height < 481) {
            CARD_HEIGHT = CGFloat(386.0)
            CARD_WIDTH = CGFloat(290.0)
        }
        // ===================================
        
        if (infoView == nil) {
            infoView = UIView(frame: CGRectMake((self.view.frame.size.width - CARD_WIDTH)/2, CARD_HEIGHT/2, CARD_WIDTH, CARD_HEIGHT))
        } else {
            for view in infoView.subviews {
                view.removeFromSuperview()
            }
        }
    
        if (isLoading) {
            loadingImage = UIImageView(image: UIImage(named: "record-fill"))
            loadingImage.frame = CGRectMake((infoView.frame.width - 50)/2, 0, 50, 50)
            let loadingLabel = UILabel(frame: CGRectMake(0, loadingImage.frame.height + 20, infoView.frame.width, 25))
            loadingImage.tintColor = UIColor.whiteColor()
            
            let stupidArray = ["Loading Sweet Tunes!", "Preparing Good Vibes!", "Getting Ready to Jam!"]
            loadingLabel.text = stupidArray[Int(arc4random_uniform(UInt32(stupidArray.count)))]
            
            loadingLabel.textColor = UIColor.whiteColor()
            loadingLabel.font = UIFont(name: "AvenirNext-Heavy", size: 20)
            loadingLabel.textAlignment = .Center
            infoView.addSubview(loadingImage)
            infoView.addSubview(loadingLabel)
            self.view.addSubview(infoView)
            return
        }
        
        let doneImage = UIImageView(image: UIImage(named: "check-large"))
        doneImage.frame = CGRectMake((infoView.frame.width - 100)/2, 0, 100, 100)
        
        let doneLabel = UILabel(frame: CGRectMake(0, doneImage.frame.height + 30, infoView.frame.width, 25))
        doneLabel.text = "You finished this session!"
        doneLabel.textColor = UIColor.whiteColor()
        doneLabel.font = UIFont(name: "AvenirNext-Heavy", size: 20)
        doneLabel.textAlignment = .Center
        
        let doneButton = UIButton(frame: CGRectMake(0, doneLabel.frame.origin.y + doneLabel.frame.height + 25, infoView.frame.width, 30))
        doneButton.setTitle("Click here to replay", forState: .Normal)
        doneButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        doneButton.setTitleColor(SoundCheckColors.yellow, forState: .Highlighted)
        doneButton.titleLabel?.font = UIFont(name: "AvenirNext-Heavy", size: 19)
        doneButton.addTarget(self, action: #selector(replaySession), forControlEvents: .TouchUpInside)
        
        infoView.addSubview(doneButton)
        infoView.addSubview(doneImage)
        infoView.addSubview(doneLabel)
        self.view.insertSubview(infoView, belowSubview: draggableViewBackground)
    }
    
    func replaySession() {
        if (replaying) {
            return
        }
        
        replaying = true
        if (draggableViewBackground != nil) {
            draggableViewBackground.removeFromSuperview()
        }
        
        self.draggableViewBackground = DraggableViewBackground(frame: self.view.frame)
        self.draggableViewBackground.frame.origin.y = -self.draggableViewBackground.frame.height
        self.draggableViewBackground.build(self)
    }
    
    func startPlaylist() {
        self.player.playSpotifyURI("spotify:user:" + uriString, startingWithIndex: 0, startingWithPosition: 0, callback: {(error: NSError!) in
            if (error != nil) {
                print("failure to play")
                return
            }
            
            UIView.animateWithDuration(0.6, animations: {
                self.draggableViewBackground.frame.origin.y = self.view.frame.origin.y
                }, completion: {_ in
                    self.createInfoView(false)
                    self.replaying = false
                    
                    if (self.dismissInProgress) {
                        self.draggableViewBackground.removeFromSuperview()
                        self.player.setIsPlaying(false, callback: { _ in
                            do {
                                try self.player.stop()
                            } catch {
                                print(error)
                            }
                        })
                    }
            })
    
            self.updateUI()
        })
    }
    
    
    // Mark - SPTAudioStreamingDelegate
    
    func audioStreamingDidLogin(audioStreaming: SPTAudioStreamingController!) {
        startPlaylist()
    }
    
    func audioStreaming(audioStreaming: SPTAudioStreamingController!, didChangePosition position: NSTimeInterval) {
        if (changingPosition || curr == nil || player == nil) {
            return
        }
        
        let progress = Float(position/self.player.metadata.currentTrack!.duration)
        
        curr.playerOptionsView.progressSlider.value = progress
        
        var seconds = String(format: "%.f", position % 60)
        var minutes = String(Int(position/60))
        
        if (seconds.characters.count == 1) {
            seconds.insert("0", atIndex: seconds.startIndex)
        }
        
        if (minutes.characters.count == 1) {
            minutes.insert("0", atIndex: minutes.startIndex)
        }
        
        if (seconds == "60") {
            seconds = "00"
            minutes = "01"
        }
        
        curr.playerOptionsView.trackTime.text = minutes + ":" + seconds
    }
    
    func audioStreaming(audioStreaming: SPTAudioStreamingController!, didChangeMetadata metadata: SPTPlaybackMetadata!) {
        if (self.loadedCards.count > 1 && !initialLoad) {
            updateUI()
        }
    }
    
    func updateLoadedCards(lc: NSMutableArray) {
        if (self.loadedCards.count >= 3) { // This value needs to be changed if we ever want less than 3 cards to be loaded
            initialLoad = false
        }
    }
    
    func updateUI() {
        if (player == nil || self.loadedCards.count <= 1 || loadedCards.count <= 1) {
            return
        }
        
        if (self.player.metadata == nil || self.player.metadata.currentTrack == nil) {
            return
        }
        
        // Figure out if user has hearted the song or not
        var imageString: String!
        
        let currentCard = loadedCards[0] as! DraggableView
        
        curr = currentCard // Set current card for audio changing
    
        if ((realm.objectForPrimaryKey(Song.self, key: self.player.metadata.currentTrack!.uri)) != nil) {
            imageString = "heart-filled"
            currentCard.playerOptionsView.hearted = true
        } else {
            imageString = "heart"
        }
        
        currentCard.playerOptionsView.icon.image = UIImage(named: imageString)
        currentCard.playerOptionsView.artistName.text = player.metadata.currentTrack?.artistName
        // Add slider controls
        currentCard.playerOptionsView.progressSlider.continuous = false
        currentCard.playerOptionsView.progressSlider.addTarget(self, action: #selector(progressSliderTouchDown), forControlEvents: UIControlEvents.TouchDown)
        currentCard.playerOptionsView.progressSlider.addTarget(self, action: #selector(seekValueChanged), forControlEvents: UIControlEvents.ValueChanged)
        
        // Get album art for next card
        if (loadedCards.count > 1 && player.metadata.nextTrack != nil) {
            let nextCard = loadedCards[1] as! DraggableView
            
            if ((realm.objectForPrimaryKey(Song.self, key: self.player.metadata.nextTrack!.uri)) != nil) {
                imageString = "heart-filled"
            } else {
                imageString = "heart"
            }
            
            nextCard.playerOptionsView.icon.image = UIImage(named: imageString)
            nextCard.playerOptionsView.artistName.text = player.metadata.nextTrack?.artistName
        }
    }
    
    // Get album art for card
    func getAlbumArt(trackURI: NSURL?, card: DraggableView) {
        if (albumImageArray[card.TAG_NUMBER] != nil) {
            card.albumImageView.image = albumImageArray[card.TAG_NUMBER]
            return
        }
        
        let auth = SPTAuth.defaultInstance()
        
        SPTTrack.trackWithURI(trackURI!, session: auth.session, callback: { error, track in
            let track = track as! SPTTrack
            let imageURL = track.album.largestCover.imageURL
            if (imageURL == nil) {
                print("album doesn't have artwork")
                return
            }
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                var image: UIImage!
                let imageData = NSData(contentsOfURL: imageURL)
                if (imageData != nil) {
                    image = UIImage(data: imageData!)
                }
                
                dispatch_async(dispatch_get_main_queue(), {
                    if (image == nil) {
                        print("Could not get image")
                        return
                    }
                    
                    if (card.TAG_NUMBER == 0) {
                        if (self.albumImageArray[0] == nil) {
                            self.albumImageArray[0] = image
                        }
                        
                        if (card.albumImageView.image != image) {
                            card.albumImageView.image = image
                        }
                        
                        return
                    }
                    
                    self.albumImageArray[card.TAG_NUMBER] = image
                    
                    if (card.albumImageView.image == UIImage(named: "placeholder")) {
                        card.albumImageView.image = self.albumImageArray[card.TAG_NUMBER]
                        card.albumImageView.alpha = 0
                        UIView.animateWithDuration(0.3, animations: {
                            card.albumImageView.alpha = 1
                        })
                    }
                })
            })
        })
    }

    // let view know position is being changed
    func progressSliderTouchDown() {
        if (curr == nil) {
            return
        }
        
        self.changingPosition = true
        if (self.player.playbackState.isPlaying == true) {
            self.player.setIsPlaying(false, callback: nil)
        }
    }
    
    func seekValueChanged() {
        if (self.curr == nil) {
            return
        }
            
        let time = Double(self.curr.playerOptionsView.progressSlider.value)
        let destination = self.player.metadata.currentTrack!.duration * time
        self.changingPosition = false
        
        self.player.seekTo(destination, callback: { error in
            if (!self.player.playbackState.isPlaying) {
                self.player.setIsPlaying(true, callback: nil)
                self.curr.playerOptionsView.playButton.setImage(UIImage(named: "pause"), forState: .Normal)
                self.curr.playerOptionsView.playButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
            }
        })
    }
    
    func hidePlayer() {
        if (loadedCards.count != 0) {
            player.setIsPlaying(false, callback: { _ in
                do {
                    try self.player.stop()
                } catch {
                    print(error)
                }
            })
        }
        
        self.draggableViewBackground.removeFromSuperview()
        
        if (self.infoView != nil) {
            self.infoView.removeFromSuperview()
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        dismissInProgress = true
        
        self.removeFromParentViewController()
        delegate.backFromSession()
    }
    
    // This is not working
    func rotateImage() {
        let rotation = CABasicAnimation(keyPath: "transform.rotation")
        rotation.fromValue = 0
        rotation.toValue = 2 * M_PI
        rotation.duration = 1.4
        rotation.repeatCount = Float.infinity
        loadingImage.layer.addAnimation(rotation, forKey: "Spin")
    }
    
    func swipeCompletion(left: Bool) {
        swipeImage = UIImageView(frame: CGRectMake(self.view.frame.width/2 - 40, self.view.frame.height/2 - 40, 80, 80))
        
        self.view.addSubview(swipeImage)
        swipeImage.bringSubviewToFront(self.view)
        swipeImage.alpha = 0.0
        
        var newSize:CGFloat = 130
        
        if (left) {
            swipeImage.image = UIImage(named: "close")
            newSize = 160
        } else {
            swipeImage.image = UIImage(named: "check-small")
        }
        
        swipeImage.tintColor = UIColor.whiteColor()
        
        UIView.animateWithDuration(0.2, animations: {
            self.swipeImage.frame = CGRectMake(self.view.frame.width/2 - newSize/2, self.view.frame.height/2 - newSize/2, newSize, newSize)
            self.swipeImage.alpha = 1.0
            }, completion: {_ in
                UIView.animateWithDuration(0.2, animations: {
                    self.swipeImage.alpha = 0.0
                    }, completion: {_ in
                        self.swipeImage.removeFromSuperview()
                })
        })
    }
}