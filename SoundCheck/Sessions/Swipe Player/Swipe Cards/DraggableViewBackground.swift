//
//  DraggableViewBackground.swift
//  SwiftTinderCards
//
//  Created by Lukasz Gandecki on 3/23/15.
//  Copyright (c) 2015 Lukasz Gandecki. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class DraggableViewBackground: UIView, DraggableViewDelegate {
    var MAX_BUFFER_SIZE: Int!
    var CARD_HEIGHT: CGFloat! //CGFloat(386.0);
    var CARD_WIDTH: CGFloat!  //CGFloat(290.0);
    
    let menuButton = UIButton()
    let messageButton = UIButton()
    let checkButton = UIButton()
    let xButton = UIButton()
    
    var numberOfCards: Int!
    var TAG_NUMBER: Int!
    
    var loadedCards: NSMutableArray!
    var allCards =  NSMutableArray()
    var cardsLoadedIndex = 0
    var numLoadedCardsCap = 0
    
    var superView = PlayerViewController()
    var player: SPTAudioStreamingController!
    
    var playlistTracks = [Card]()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        CARD_HEIGHT = CGFloat(self.frame.height/1.50)
        CARD_WIDTH = CGFloat(self.frame.width/1.1)
        
        // Set height for iPhone 5/4
        if (self.frame.height < 481) {
            CARD_HEIGHT = CGFloat(386.0)
            CARD_WIDTH = CGFloat(290.0)
        }
        
        super.layoutSubviews()
        if (self.player == nil) {
            self.player = SPTAudioStreamingController.sharedInstance()
        }
    }
    
    func build(superView: PlayerViewController) {
        self.superView = superView
        self.loadedCards = superView.loadedCards
        getPlaylistInfo()
    }
    
    func setupView() {
        setBackgroundColor()
    }
    
    func setBackgroundColor() {
        self.backgroundColor = UIColor.clearColor()
    }
    
    func setLoadedCardsCap() {
        numLoadedCardsCap = 0;
        if (numberOfCards > MAX_BUFFER_SIZE) {
            numLoadedCardsCap = MAX_BUFFER_SIZE
        } else {
            numLoadedCardsCap = numberOfCards
        }
        
    }
    
    func createCards() {
        if (numLoadedCardsCap > 0) {
            let cardFrame = CGRectMake((self.frame.size.width - CARD_WIDTH)/2, (self.frame.size.height - CARD_HEIGHT)/2, CARD_WIDTH, CARD_HEIGHT)
            
            for _ in 0...numberOfCards {
                let newCard = DraggableView(frame: cardFrame, information: "Nothing")
                newCard.player = self.player
                newCard.delegate = self;
                allCards.addObject(newCard)
            }
        }
    }

    func displayCards() {
        for i in 0...numLoadedCardsCap {
            loadACardAt(i)
        }
    }
    
    func cardSwipedLeft(card: DraggableView) {
        processCardSwipe()
        superView.swipeCompletion(true)
        player.skipNext(nil)
    }
    
    func cardSwipedRight(card: DraggableView) {
        processCardSwipe()
        superView.swipeCompletion(false)
        
        let backgroundQueue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0)
        dispatch_async(backgroundQueue, {
            self.addToCollectionView(self.player.metadata.currentTrack!.uri, songName: card.playerOptionsView.trackTitle.text!, songArtist: card.playerOptionsView.artistName.text!, songArt: card.albumImageView.image!)
        })
        
        player.skipNext(nil)
    }
    
    // Add to collection view
    func addToCollectionView(uri: String, songName: String, songArtist: String, songArt: UIImage) {
        let realm = try! Realm()
        
        if (realm.objectForPrimaryKey(Song.self, key: uri) != nil) {
            return
        }
        
        try! realm.write {
            let song = Song()
            song.uri = uri
            
            let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
            let writePath = documentsPath.stringByAppendingString("/" + song.uri)
            let imageData: NSData = UIImagePNGRepresentation(songArt)!
            
            imageData.writeToFile(writePath, atomically: true)
            
            song.artPath = imageData
            song.name = songName
            song.artist = songArtist
            realm.add(song)
        }
    }
    
    func processCardSwipe() {
        loadedCards.removeObjectAtIndex(0)
        
        if (loadedCards.count > 0) {
            let frontCard = loadedCards[0] as! DraggableView
            frontCard.userInteractionEnabled = true
        }
        
        if (moreCardsToLoad()) {
            loadNextCard()
        } else if (loadedCards.count == 0) {
            player.setIsPlaying(false, callback: { _ in
                do {
                    try self.player.stop()
                    self.removeFromSuperview()
                } catch {
                    print(error)
                }
            })
        }
    }
    
    func moreCardsToLoad() -> Bool {
        return cardsLoadedIndex < allCards.count;
    }
    
    func loadNextCard() {
        loadACardAt(cardsLoadedIndex)
    }
    
    func loadACardAt(index: Int) {
        loadedCards.addObject(allCards[index])
        if (loadedCards.count > 1) {
            let card = loadedCards[loadedCards.count-1] as! DraggableView
            card.TAG_NUMBER = cardsLoadedIndex
            insertSubview(card, belowSubview: loadedCards[loadedCards.count-2] as! DraggableView)
            // is there a way to define the array with UIView elements so I don't have to cast?
        } else {
            let firstCard = loadedCards[0] as! DraggableView
            firstCard.userInteractionEnabled = true
            firstCard.TAG_NUMBER = cardsLoadedIndex
            addSubview(loadedCards[0] as! DraggableView)
        }
        
        self.superView.updateLoadedCards(loadedCards)
        
        cardsLoadedIndex += 1;
    }
    
    // Load playlist
    func getPlaylistInfo() {
        let auth = SPTAuth.defaultInstance()
        
        SPTPlaylistSnapshot.playlistWithURI(NSURL(string: "spotify:user:" + superView.uriString), session: auth.session, callback: { error, playlist in
            if (error != nil) {
                print("error fetching playlist")
                return
            }
            
            let p = playlist as! SPTPlaylistSnapshot
            let tracks = p.firstTrackPage
            
            self.MAX_BUFFER_SIZE = Int(tracks.totalListLength) - 2
            self.numberOfCards = Int(tracks.totalListLength) - 2
            self.TAG_NUMBER = Int(tracks.totalListLength) - 2

            self.setLoadedCardsCap()
            self.setupView()
            self.createCards()
            self.displayCards()
            
            for i in 0...2 {
                let s = tracks.items[i] as! SPTPlaylistTrack
                let card = Card(trackTitle: s.name, artistName: "", uri: s.uri)
                self.playlistTracks.append(card)
                let loadedCard = self.loadedCards[i] as! DraggableView
                loadedCard.playerOptionsView.artistName.text = card.artistName
                loadedCard.playerOptionsView.trackTitle.text = card.trackTitle
                self.superView.getAlbumArt(s.uri, card: loadedCard)
            }
            
            for i in 2...self.numberOfCards {
                let s = tracks.items[i] as! SPTPlaylistTrack
                let card = Card(trackTitle: s.name, artistName: "", uri: s.uri)
                self.playlistTracks.append(card)
                let loadedCard = self.loadedCards[i] as! DraggableView
                loadedCard.playerOptionsView.artistName.text = card.artistName
                loadedCard.playerOptionsView.trackTitle.text = card.trackTitle
                self.superView.getAlbumArt(s.uri, card: loadedCard)
            }
            
            self.superView.handleNewSession()
            return
        })
    }
    
    func swipeRight() {
        let dragView = loadedCards[0] as! DraggableView
        print ("Clicked right", terminator: "")
        dragView.rightClickAction()
    }
    
    func swipeLeft() {
        let dragView = loadedCards[0] as! DraggableView
        print ("clicked left", terminator: "")
        dragView.leftClickAction()
    }
}

class Card {
    var trackTitle: String!
    var artistName: String!
    var uri: NSURL!
    var albumImage: UIImage?
    
    init(trackTitle: String, artistName: String, uri: NSURL) {
        self.artistName = artistName
        self.trackTitle = trackTitle
        self.uri = uri
        self.albumImage = nil
    }

    func getAlbumArt() {
        
    }
    
}
