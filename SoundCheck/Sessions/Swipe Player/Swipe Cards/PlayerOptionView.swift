//
//  PlayerOptionView.swift
//  SwipeMusic
//
//  Created by Jake on 9/11/16.
//  Copyright © 2016 Jake. All rights reserved.
//

import UIKit
import RealmSwift

class PlayerOptionView: UIView {

    var hearted = false
    
    let realm = try! Realm()
    let artistName = UILabel()
    let trackTitle = UILabel()
    let trackTime = UILabel()
    let playButton = UIButton()
    var icon: UIImageView!
    let progressSlider = ProgressSlider()
    var panGestureRecognizer: UIPanGestureRecognizer!
    var player: SPTAudioStreamingController!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func build() {
        let padding: CGFloat = 10
        
        self.backgroundColor = SoundCheckColors.darkGray
        
        self.playButton.frame = CGRectMake(padding, padding, self.frame.height - 20, self.frame.height - 20)
        self.playButton.layer.cornerRadius = playButton.frame.width/2
        self.playButton.layer.borderColor = UIColor.whiteColor().CGColor
        self.playButton.layer.borderWidth = 1
        self.playButton.imageView!.contentMode = UIViewContentMode.ScaleAspectFit
        self.playButton.setImage(UIImage(named: "pause"), forState: .Normal)
        self.playButton.addTarget(self, action: #selector(playPause), forControlEvents: .TouchUpInside)
        
        self.trackTitle.frame = CGRectMake(padding*2 + playButton.frame.width, padding, 0, 20)
        self.trackTitle.frame.size.width = self.frame.width - self.trackTitle.frame.origin.x - padding
        self.trackTitle.textColor = UIColor.whiteColor()
        self.trackTitle.text = "Track Title"
        self.trackTitle.numberOfLines = 1
        self.trackTitle.adjustsFontSizeToFitWidth = true
        
        self.artistName.frame = CGRectMake(self.trackTitle.frame.origin.x + 1, self.trackTitle.frame.origin.y + self.trackTitle.frame.height + 2, self.trackTitle.frame.width, 15)
        self.artistName.font = UIFont.systemFontOfSize(14)
        self.artistName.textColor = UIColor(red:0.63, green:0.63, blue:0.63, alpha:1.0)
        self.artistName.numberOfLines = 1
        self.artistName.adjustsFontSizeToFitWidth = true
        self.artistName.text = "Artist Name"
        
        self.progressSlider.build(panGestureRecognizer)
        self.progressSlider.frame = CGRectMake(self.trackTitle.frame.origin.x, self.frame.height - 25, self.trackTitle.frame.width - 60, 5)
        self.progressSlider.minimumTrackTintColor = SoundCheckColors.yellow
        self.progressSlider.setThumbImage(UIImage(named: "slider")!, forState: .Normal)
        self.progressSlider.tintColor = SoundCheckColors.yellow
        
        self.trackTime.frame = CGRectMake(self.trackTitle.frame.origin.x + self.progressSlider.frame.width + self.trackTime.frame.width + 5, self.frame.height - 30, 30, 20)
        self.trackTime.text = "00:00"
        self.trackTime.font = UIFont.systemFontOfSize(10)
        self.trackTime.textColor = UIColor(red:0.63, green:0.63, blue:0.63, alpha:1.0)
        
        self.icon = UIImageView(frame: CGRectMake(self.trackTitle.frame.origin.x + self.progressSlider.frame.width + self.trackTime.frame.width + padding + 2, self.trackTime.frame.origin.y, 20, 20))
        self.icon.image = UIImage(named: "heart")
        icon.tintColor = UIColor.whiteColor()
        
//        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(changeHeart))
//        self.icon.userInteractionEnabled = true
//        self.icon.addGestureRecognizer(tapGestureRecognizer)
        
        self.addSubview(trackTime)
        self.addSubview(icon)
        self.addSubview(progressSlider)
        self.addSubview(artistName)
        self.addSubview(trackTitle)
        self.addSubview(playButton)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        if (gestureRecognizer == panGestureRecognizer) {
            return false
        }
        
        return true
    }
    
    func playPause() {
        if (self.player == nil) {
            self.player = SPTAudioStreamingController.sharedInstance()
        }
        
        // Stop song from play/pausing if it hasn't loaded yet
        if (player.metadata == nil || player.metadata.currentTrack == nil) {
            return
        }
        
        if (self.player.playbackState.isPlaying) {
            self.playButton.setImage(UIImage(named: "play"), forState: .Normal)
            self.playButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -7) // center button image. TODO - Fix for all screen sizes
        } else {
            self.playButton.setImage(UIImage(named: "pause"), forState: .Normal)
            self.playButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        }
        
        self.player.setIsPlaying(!player.playbackState.isPlaying, callback: nil)
    }
    
    func changeHeart() {
        if (self.player == nil) {
            self.player = SPTAudioStreamingController.sharedInstance()
        }

        if (realm.objectForPrimaryKey(Song.self, key: player.metadata.currentTrack!.uri) == nil) {
            try! realm.write {
                let songObject = Song()
                songObject.uri = player.metadata.currentTrack!.uri
                realm.add(songObject)
            }
        }
        
        let song = realm.objectForPrimaryKey(Song.self, key: player.metadata.currentTrack!.uri)
        let user = realm.objectForPrimaryKey(User.self, key: 0)
        
        if (self.hearted) {
            self.icon.image = UIImage(named: "heart")
            try! realm.write {
                realm.delete(song!)
            }
        } else {
            self.icon.image = UIImage(named: "heart-filled")
            try! realm.write {
                user!.songs.append(song!)
            }
        }
    
        self.hearted = !self.hearted
    }
}

// Mark - Subclassing so Objects ignore the panGestureRecognizer

class ProgressSlider: UISlider {
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    func build(pgr: UIPanGestureRecognizer) {
        self.panGestureRecognizer = pgr
    }
    
    override func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        if (gestureRecognizer == panGestureRecognizer) {
            return false
        }
        
        return true
    }
}
