//
//  ViewController.swift
//  SoundCheck
//
//  Created by Jake on 9/22/16.
//  Copyright © 2016 Jake. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController, SPTAuthViewDelegate {
    
    var authViewController: SPTAuthViewController!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = SoundCheckColors.backgroundBlack
        
        loginButton.layer.cornerRadius = loginButton.frame.height/2
        loginButton.setTitle("Login with Spotify", forState: .Normal)
        loginButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        loginButton.titleLabel!.font = UIFont(name: "AvenirNext", size: 19)
        loginButton.backgroundColor = SoundCheckColors.spotifyGreen
    }
    
    
    @IBAction func login(sender: AnyObject) {
        self.openLoginPage()
    }
    
    
    // Mark - SPTAuthViewDelegate
    
    func authenticationViewControllerDidCancelLogin(authenticationViewController: SPTAuthViewController!) {
        print("Login Canceled")
    }
    
    func authenticationViewController(authenticationViewController: SPTAuthViewController!, didLoginWithSession session: SPTSession!)
    {
        performSegueWithIdentifier("loginSuccessSegue", sender: self)
    }
    
    func authenticationViewController(authenticationViewController: SPTAuthViewController!, didFailToLogin error: NSError!) {
        print(error)
        print("error")
    }
    
    func openLoginPage() {
        self.authViewController = SPTAuthViewController.authenticationViewController()
        self.authViewController.delegate = self
        
        SPTAuth.defaultInstance().allowNativeLogin = false // Do not user login through spotify
        
//        self.authViewController.clearCookies(nil) // Clear cookies should be moved later
        
        self.authViewController.modalPresentationStyle = .OverCurrentContext
        self.authViewController.modalTransitionStyle = .CrossDissolve
        
        self.modalPresentationStyle = .CurrentContext
        self.definesPresentationContext = true
        self.presentViewController(authViewController, animated: false, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // This is getting ridiculous...this whole app needs to get rebuilt
        let realm = try! Realm()
        if (realm.objectForPrimaryKey(User.self, key: 0) == nil) {
            let user = User()
            user.id = 0
            realm.add(user)
        }
    }
}

