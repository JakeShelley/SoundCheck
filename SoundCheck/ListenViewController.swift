//
//  ListenViewController.swift
//  SoundCheck
//
//  Created by Jake on 10/4/16.
//  Copyright © 2016 Jake. All rights reserved.
//

import UIKit

class ListenViewController: UIViewController, SPTAudioStreamingDelegate, SPTAudioStreamingPlaybackDelegate {
    
    var player: SPTAudioStreamingController!
    
    var changingPosition = false
    
    var artistName: UILabel!
    var trackTitle: UILabel!
    
    var albumArtView: UIImageView!
    var progressSlider: UISlider!
    var playButton: UIButton!
    var trackTime: UILabel!
    
    var songIndex: Int!
    
    var songList: [Song]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = SoundCheckColors.backgroundBlack
        setUpView()
        setUpControls()
        handleNewSession()
    }

    func setUpView() {
        let albumMeasurement = CGFloat(self.view.frame.width - 30)
        albumArtView = UIImageView(frame: CGRectMake((self.view.frame.size.width - albumMeasurement)/2, 75, albumMeasurement, albumMeasurement))
        albumArtView.backgroundColor = SoundCheckColors.gray
        
        let xButton = UIButton(frame: CGRectMake(albumArtView.frame.origin.x - 12, 30, 40, 40))
        xButton.setBackgroundImage(UIImage(named: "close"), forState: .Normal)
        xButton.addTarget(self, action: #selector(closePlayer), forControlEvents: .TouchUpInside)
        
        self.view.addSubview(xButton)
        self.view.addSubview(albumArtView)
    }
    
    func setUpControls() {
        let albumFrame = albumArtView.frame
        let controlView = UIView(frame: CGRectMake(0, albumFrame.origin.y + albumFrame.height, self.view.frame.width, self.view.frame.height - (albumFrame.origin.y + albumFrame.height)))
        controlView.backgroundColor = UIColor.clearColor()
        
        trackTitle = UILabel(frame: CGRectMake(0, 15, self.view.frame.width, 20))
        trackTitle.text = songList[songIndex].name
        trackTitle.textAlignment = .Center
        trackTitle.textColor = UIColor.whiteColor()
        
        artistName = UILabel(frame: CGRectMake(0, trackTitle.frame.origin.y + 20, self.view.frame.width, 20))
        artistName.text = songList[songIndex].artist
        artistName.textAlignment = .Center
        artistName.font = UIFont.systemFontOfSize(14)
        artistName.textColor = UIColor.whiteColor()
        
        trackTime = UILabel(frame: CGRectMake(15, artistName.frame.origin.y + 37, 30, 10))
        trackTime.font = UIFont.systemFontOfSize(10)
        trackTime.text = "00:00"
        trackTime.textColor = UIColor.whiteColor()
        
        progressSlider = UISlider(frame: CGRectMake(25 + trackTime.frame.width, trackTime.frame.origin.y + 2, albumFrame.width - trackTime.frame.width - 25, 5))
        progressSlider.minimumTrackTintColor = SoundCheckColors.yellow
        progressSlider.setThumbImage(UIImage(named: "slider")!, forState: .Normal)
        self.progressSlider.tintColor = SoundCheckColors.yellow
        progressSlider.continuous = false
        progressSlider.addTarget(self, action: #selector(progressSliderTouchDown), forControlEvents: .TouchDown)
        progressSlider.addTarget(self, action: #selector(seekValueChanged), forControlEvents: UIControlEvents.ValueChanged)
        
        playButton = UIButton(frame: CGRectMake((self.view.frame.width - 60)/2, progressSlider.frame.origin.y + 40, 65, 65))
        playButton.layer.cornerRadius = playButton.frame.width/2
        playButton.layer.borderColor = UIColor.whiteColor().CGColor
        playButton.layer.borderWidth = 1
        playButton.imageView!.contentMode = UIViewContentMode.ScaleAspectFit
        playButton.setImage(UIImage(named: "pause"), forState: .Normal)
        playButton.addTarget(self, action: #selector(playPause), forControlEvents: .TouchUpInside)
        
        let skipBack = UIButton(frame: CGRectMake((self.view.frame.width - 40)/2 - 67, playButton.frame.origin.y + playButton.frame.height/2 - 20, 40, 40))
        skipBack.setBackgroundImage(UIImage(named: "rewind"), forState: .Normal)
        skipBack.addTarget(self, action: #selector(skipBackward), forControlEvents: .TouchUpInside)
        
        let skip = UIButton(frame: CGRectMake((self.view.frame.width - 40)/2 + 70, playButton.frame.origin.y + playButton.frame.height/2 - 20, 40, 40))
        skip.setBackgroundImage(UIImage(named: "fast-forward"), forState: .Normal)
        skip.addTarget(self, action: #selector(skipForward), forControlEvents: .TouchUpInside)
        
        albumArtView.image = UIImage(data: songList[songIndex].artPath) //UIImage(contentsOfFile: songList[songIndex].artPath)
        
        controlView.addSubview(trackTitle)
        controlView.addSubview(artistName)
        controlView.addSubview(skip)
        controlView.addSubview(skipBack)
        controlView.addSubview(trackTime)
        controlView.addSubview(progressSlider)
        controlView.addSubview(playButton)
        self.view.addSubview(controlView)
    }
    
    func handleNewSession() {
        let auth = SPTAuth.defaultInstance()
        
        if (player == nil) {
            player = SPTAudioStreamingController.sharedInstance()
        }
        
        player.delegate = self
        player.playbackDelegate = self
        
        do {
            try self.player.startWithClientId(auth.clientID, audioController: nil, allowCaching: true)
            self.player.delegate = self
            self.player.playbackDelegate = self
            self.player.diskCache = SPTDiskCache(capacity: 1024 * 1024 * 64)
            self.player.loginWithAccessToken(auth.session.accessToken)
        } catch {
            // Could not start up (improve error handling)
            print("error could not start client")
        }
    }
    
    func startPlaylist(songURI: String) {
        self.player.playSpotifyURI(songURI, startingWithIndex: 0, startingWithPosition: 0, callback: {(error: NSError!) in
            if (error != nil) {
                print("failure to play")
                return
            }
            
            let queueSong: String!
            
            if (self.songList.count <= self.songIndex) {
                queueSong = self.songList[0].uri
            } else if (self.songIndex < 0) {
                queueSong = self.songList[self.songList.count - 1].uri
            } else {
                queueSong = self.songList[self.songIndex].uri
            }
         
            self.updateUI()
            
            self.player.queueSpotifyURI(queueSong, callback: nil)
        })
    }
    
    func skipForward() {
        if (self.player == nil) {
            return
        }
        
        songIndex = songIndex + 1
        
        if (!player.playbackState.isPlaying) {
            playPause()
        }
        
        if (songList.count <= songIndex) {
            songIndex = 0
        }
        
        startPlaylist(songList[songIndex].uri)
    }
    
    func skipBackward() {
        if (self.player == nil) {
            return
        }
        
        if (player.playbackState.position > 1) {
            progressSlider.value = 0
            seekValueChanged()
            return
        }
        
        songIndex = songIndex - 1
        
        let prevSong: String!
        
        if (songIndex < 0) {
            songIndex = songList.count - 1
            prevSong = songList[songIndex].uri
        } else {
            prevSong = songList[songIndex].uri
        }
        
        startPlaylist(prevSong)
    }
    
    // Mark - SPTAudioStreamingDelegate
    
    func audioStreamingDidLogin(audioStreaming: SPTAudioStreamingController!) {
        startPlaylist(songList[songIndex].uri)
    }
    
    func audioStreaming(audioStreaming: SPTAudioStreamingController!, didChangePosition position: NSTimeInterval) {
        if (changingPosition || player == nil) {
            return
        }
        
        let progress = Float(position/self.player.metadata.currentTrack!.duration)
        
        progressSlider.value = progress
        
        var seconds = String(format: "%.f", position % 60)
        var minutes = String(Int(position/60))
        
        if (seconds.characters.count == 1) {
            seconds.insert("0", atIndex: seconds.startIndex)
        }
        
        if (minutes.characters.count == 1) {
            minutes.insert("0", atIndex: minutes.startIndex)
        }
        
        if (seconds == "60") {
            seconds = "00"
            minutes = "01"
        }
        
        trackTime.text = minutes + ":" + seconds
    }
    
    func audioStreaming(audioStreaming: SPTAudioStreamingController!, didChangeMetadata metadata: SPTPlaybackMetadata!) {
        
    }
    
    func audioStreaming(audioStreaming: SPTAudioStreamingController!, didStopPlayingTrack trackUri: String!) {
        songIndex = songIndex + 1
        if (songList.count <= songIndex) {
            startPlaylist(songList[0].uri)
            return
        }
        
        startPlaylist(songList[songIndex].uri)
    }

    func playPause() {
        if (self.player == nil) {
            return
        }
        
        // Stop song from play/pausing if it hasn't loaded yet
        if (player.metadata == nil || player.metadata.currentTrack == nil) {
            return
        }
        
        if (self.player.playbackState.isPlaying) {
            self.playButton.setImage(UIImage(named: "play"), forState: .Normal)
            self.playButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -7) // center button image. TODO - Fix for all screen sizes
        } else {
            self.playButton.setImage(UIImage(named: "pause"), forState: .Normal)
            self.playButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        }
        
        self.player.setIsPlaying(!player.playbackState.isPlaying, callback: nil)
    }
    
    // let view know position is being changed
    func progressSliderTouchDown() {
        if (player == nil) {
            return
        }
        
        changingPosition = true
        if (self.player.playbackState.isPlaying == true) {
            player.setIsPlaying(false, callback: nil)
        }
    }
    
    func seekValueChanged() {
        let time = Double(progressSlider.value)
        let destination = player.metadata.currentTrack!.duration * time
        changingPosition = false
        
        self.player.seekTo(destination, callback: { error in
            if (!self.player.playbackState.isPlaying) {
                self.player.setIsPlaying(true, callback: nil)
                self.playButton.setImage(UIImage(named: "pause"), forState: .Normal)
                self.playButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
            }
        })
    }
    
    func closePlayer() {
        player.setIsPlaying(false, callback: {_ in
            try! self.player.stop()
        })
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func updateUI() {
        albumArtView.image = UIImage(data: songList[songIndex].artPath) // UIImage(contentsOfFile: songList[songIndex].artPath)
        trackTitle.text = songList[songIndex].name
        artistName.text = songList[songIndex].artist
    }
}
